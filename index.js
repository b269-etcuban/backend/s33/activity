fetch('https://jsonplaceholder.typicode.com/todos')
.then(response => response.json())
.then((json) => console.log(json));

fetch('https://jsonplaceholder.typicode.com/todos')
  .then(response => response.json())
  .then(data => {
    let title = data.map(item => item.title);
    console.log(title);
  })

fetch('https://jsonplaceholder.typicode.com/todos/1')
.then(response => response.json())
.then(data => console.log(data))

fetch('https://jsonplaceholder.typicode.com/todos/1')
  .then(response => response.json())
  .then(data => {
    const { title, completed } = data;
    console.log(`The item "${title}" on the list has a status of ${completed} script.`);
  })

fetch('https://jsonplaceholder.typicode.com/todos', {
	method: 'POST',
	headers: {'Content-Type': 'application/json'},
	body: JSON.stringify({
  		title: 'Created To Do List Item',
  		userId: 1,
  		completed: false
	})
})
.then(response => response.json())
.then(data => console.log(data))

fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PUT',
	headers: {'Content-Type': 'application/json'},
	body: JSON.stringify({
  		title: 'Updated To Do List Item',
  		description: 'To upate the my to do list with a different data structure',
  		userId: 1,
  		completed: "pending"
	})
})
.then(response => response.json())
.then(data => console.log(data))


fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PATCH',
	headers: {'Content-Type': 'application/json'},
	body: JSON.stringify({
  		dateCompleted:  "07/09/21",
  		status: "Complete"
	})
})
.then(response => response.json())
.then(data => console.log(data))

fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'DELETE'
})